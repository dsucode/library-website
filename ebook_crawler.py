from videoserver.models import LibraryEbooks, DeptId
import os
from shutil import copyfile

# nptel id for cse is 106
# first we will copy pdf from one source to another, while transferring it to other source, change the file extension also

def get_all_files(file_dir):
    for root, dirs, files in os.walk(file_dir, topdown=False):
        for name in files:
            #print(os.path.join(root, name))
            #print name
            #we want to copy this file to 'videoserver/static/sda2/ebooks'
	    file_name_new = copy_file(name, 'videoserver/static/sda2/ebooks/')

	    #now we need to insert it into database
	    new_ebook = LibraryEbooks()

	    new_ebook.dept_id = DeptId.objects.get(nptel_id=903)
	    new_ebook.ebook_name = file_name_new[:-4]
	    new_ebook.ebook_file_location = 'sda2/ebooks/' + file_name_new
	    new_ebook.ebook_thumbnail_image_path = ''

	    new_ebook.save()

	    print "file: " + file_name_new + " success!"


def copy_file(file_name, destination):

    #this utility will also fix file names.
    #first it will copy then it will save entry to database

    file_name_new = file_name.replace(' ', '_').replace('-', '_')

    copyfile('/media/dsu/DSU/E-Books/Physics/' + file_name, destination + file_name_new)

    return file_name_new


get_all_files("/media/dsu/DSU/E-Books/Physics/")
