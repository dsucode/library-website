import requests
from bs4 import BeautifulSoup
import json
from .models import DeptId

nptel_url = 'http://nptel.ac.in/'

r = requests.get(nptel_url)

bsObj = BeautifulSoup(r.text)

deptName = bsObj.findAll('div', {"class":"individual_dept"})

for i in range(len(deptName)):
    #import pdb; pdb.set_trace()
    print deptName[i].get_text().strip()
    
    DeptId.objects.create(dept_name=deptName[i].get_text().strip(),
    nptel_id=deptName[i].find('a').get('href')[-3:])
    
    print deptName[i].find('a').get('href')[-3:]
    
