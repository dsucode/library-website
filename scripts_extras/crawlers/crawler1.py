import requests
from bs4 import BeautifulSoup
import json
from .models import Course, DeptId

course_catalog_url = 'http://nptel.ac.in/server.php'
    
def getCourseData(dispid, limit, deptObj):
    for i in range(limit):
        r = requests.post(course_catalog_url, data={'search_dispid':dispid, 'offset':i+1})
        try:
            json_data = json.loads(r.text)
            for i in range(len(json_data)):
            
                if json_data[i]['ContentType'] == 'Video':
                    print json_data[i]['subjectId']
                    print json_data[i]['subjectName']
                    Course.objects.create(course_id=json_data[i]['subjectId'],
                    course_name=json_data[i]['subjectName'], dept_id=deptObj)
                    
        except ValueError:
            print("Unable to get JSON data for id " + str(i))
        except KeyError:
            print("Unable to parse json data for " + i)
            
courseId = 112
getCourseData(courseId, 15, DeptId.objects.get(nptel_id=courseId))
