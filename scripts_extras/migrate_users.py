from videoserver.models import Users
from django.contrib.auth.models import User

for i in range(408, 412):
    user = Users.objects.get(id=i)
    if user.email:
        user.user_id = user.email.split('@')[0]
        user.save()
        migrate_user = User.objects.create_user(user.user_id, user.email, 'password')
        migrate_user.save()