import requests
from bs4 import BeautifulSoup
import django
import os
import time
from openpyxl import load_workbook

wb = load_workbook(filename='Online Journals List DSU Library.xlsx')
f = open("names.txt",'w')
ws = wb['J-Gate e-journals']
f.write("<table>"+"\n")

for i in range(2,101):

    if ws['B'+str(i)].value:
        f.write("<tr><td><a target='_blank' href='https://www.google.co.in/search?q="+ws['B'+str(i)].value.replace(' ','+')+"'>"+ws['B'+str(i)].value+"</a></td></tr>"+"\n")

f.write("</table>")
  