import requests
from bs4 import BeautifulSoup
import django
import os

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "library.settings")
django.setup()

from videoserver.models import Course, DeptId, Topics


#print Course.objects.filter(dept_id__nptel_id=106)

base_url = 'http://nptel.ac.in/courses/'

video_url = base_url + Course.objects.filter(dept_id__nptel_id=106)[0].course_id + '/'

def download_url(video_url):
    r = requests.get(video_url)
    bsObj = BeautifulSoup(r.text, 'html5lib')
    print bsObj.find('div', {"id":"download"}).find('a').get('href')
    


download_url(video_url)


