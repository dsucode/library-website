import requests
from bs4 import BeautifulSoup
import django
import os

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "library.settings")
django.setup()

from videoserver.models import Course, DeptId, Topics

def fill_db(course_name, i):
    counter = 3
    url_counter = 1
    while counter:
        video_url = 'http://nptel.ac.in/courses/' + i.course_id + '/' + str(url_counter)
        r = requests.get(video_url)
        bsObj = BeautifulSoup(r.text, 'html5lib')
        try:
            print bsObj.find('a', {"id":"lecturename"}).get_text()
            print bsObj.find('div', {"id":"download"}).find('a').get('href')
            #Topics.objects.topic_name = bsObj.find('a', {"id":"lecturename"}).get_text()
            #Topics.objects.course_id = course_id
            #Topics.objects.video_download_url = bsObj.find('div', {"id":"download"}).find('a').get('href')
            Topics.objects.create(topic_name=bsObj.find('a', {"id":"lecturename"}).get_text(),
            course_id=i, video_download_url=bsObj.find('div', {"id":"download"}).find('a').get('href'))
        except AttributeError:
            print "No more value found!"
            counter = counter - 1
        url_counter = url_counter + 1

#print Course.objects.filter(dept_id__nptel_id=106) then do 108 because Prachi likes electrical, god knows why
#for i in Course.objects.filter(dept_id__nptel_id=106):
#    fill_db(i.course_name, i)

#for i in Course.objects.filter(dept_id__nptel_id=117):
#    fill_db(i.course_name, i)

#for i in Course.objects.filter(dept_id__nptel_id=108):
#    fill_db(i.course_name, i)

for i in Course.objects.filter(dept_id__nptel_id=112):
    fill_db(i.course_name, i)
