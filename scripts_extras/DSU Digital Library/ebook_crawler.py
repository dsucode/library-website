from videoserver.models import LibraryEbooks, DeptId

import os

counter = 0

for root, dirs, files in os.walk("videoserver/static/ebooks/ebooks/gs/", topdown=False):
    for name in files:
        #print(os.path.join(root, name))
        print name

        name = name[:60]

        ebook_obj = LibraryEbooks()

        ebook_obj.dept_id = DeptId.objects.get(nptel_id=904)
        ebook_obj.ebook_name = name
        ebook_obj.ebook_file_location = 'ebooks/ebooks/gs/' + name.replace(' ', '_').replace('-', '_')

        ebook_obj.save()
