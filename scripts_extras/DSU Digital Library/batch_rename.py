import os

counter = 0

for root, dirs, files in os.walk("videoserver/static/ebooks/ebooks/gs/", topdown=False):
    for name in files:
        print(os.path.join(root, name))
        print name

        old_name = os.path.join(root, name)
        new_name = os.path.join(root, name.replace(' ', '_').replace('-', '_'))

        os.rename(old_name, new_name)
