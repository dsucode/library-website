import requests
from bs4 import BeautifulSoup
import django
import os
import time

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "library.settings")
django.setup()

from videoserver.models import Course, DeptId, Topics

dept_id_dict = [106, 117, 108, 112]

def path_name_builder(path_name):
    pass

def create_directory(path_name):
    if not os.path.exists(path_name):
        os.makedirs(path_name)

def download_file(url, file_name, counter):
    local_file_name = file_name + 'lec' + str(counter) + '.mp4'
    print local_file_name
    #local_filename = url.split('/')[-1]
    # NOTE the stream=True parameter
    try:
        r = requests.get(url, stream=True)
        with open(local_file_name, 'wb') as f:
            for chunk in r.iter_content(chunk_size=1024): 
                if chunk: # filter out keep-alive new chunks
                    f.write(chunk)
                    #f.flush() commented by recommendation from J.F.Sebastian
    except requests.exceptions.RequestException as e:
        print e
        print "trying one more then after 10 minutes"
        time.sleep(300)

        try:
            r = requests.get(url, stream=True)
            with open(local_file_name, 'wb') as f:
                for chunk in r.iter_content(chunk_size=1024): 
                    if chunk: # filter out keep-alive new chunks
                        f.write(chunk)
                        #f.flush() commented by recommendation from J.F.Sebastian
        except requests.exceptions.RequestException as e:
            print e
            print "Downloading another video."
            with open('download_error_log.log', 'a') as error_log:
                error_log.write(url)
                error_log.write('\n')

    
for i in dept_id_dict:
    #print i
    #create_directory(i)
    for j in Course.objects.filter(dept_id__nptel_id=i):
        #print j.course_id
        file_name = 'static/nptel_videos/' + str(i) + '/' + str(j.course_id) + '/'
        create_directory(file_name)
        
        counter = 1
        
        for k in Topics.objects.filter(course_id__course_id=j.course_id).order_by('id'):
            print k.topic_name
            print k.video_download_url
            if k.download_complete == False:
                download_file(k.video_download_url, file_name, counter)
                k.video_location  = file_name + 'lec' + str(counter) + '.mp4'
                k.download_complete = True
                k.save()

            else:
                print k.topic_name + "  video already downloaded"

            counter = counter + 1