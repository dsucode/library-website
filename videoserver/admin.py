from django.contrib import admin

# Register your models here.

from .models import Users, Quotes, DeptId, LibraryUsers

# Register your models here.
admin.site.register(Users)
admin.site.register(Quotes)
admin.site.register(LibraryUsers)

from .models import LibraryUsers, Quotes, DeptId, Topics, Counter, LibraryEbooks, UserFiles

# Register your models here.
admin.site.register(Topics)
admin.site.register(DeptId)
admin.site.register(LibraryUsers)
admin.site.register(Quotes)
admin.site.register(Counter)
admin.site.register(LibraryEbooks)
admin.site.register(UserFiles)
