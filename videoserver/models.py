from __future__ import unicode_literals

from django.db import models
from django.contrib.auth.models import User

# Create your models here.

class DeptId(models.Model):
    dept_name = models.CharField(max_length=200)
    nptel_id = models.CharField(max_length=50)
    
    def __unicode__(self):
        return self.dept_name
    
class Course(models.Model):
    course_name = models.CharField(max_length=200)
    course_id = models.CharField(max_length=50, default=0)
    dept_id = models.ForeignKey(DeptId)
    
    def __unicode__(self):
        return self.course_name
    
class Topics(models.Model):
    topic_name = models.CharField(max_length=300)
    course_id = models.ForeignKey(Course)
    video_download_url = models.CharField(max_length=500, default='')
    video_location = models.CharField(max_length=300)
    new_video_location = models.CharField(max_length=300, default='')
    download_complete = models.BooleanField(default=False)

    def __unicode__(self):
        return self.topic_name


class Users(models.Model):
    userid = models.ForeignKey(User, null=True)
    name = models.CharField(max_length=100)
    user_id = models.CharField(max_length=50, blank=True)
    id_code = models.CharField(max_length=50)
    category = models.CharField(max_length=50)
    group = models.CharField(max_length=50)
    date_of_birth = models.CharField(max_length=40, null=True)
    gender = models.CharField(max_length=10)
    email = models.CharField(max_length=50, null=True)
    telephone = models.CharField(max_length=40, null=True)

    def __unicode__(self):
        return self.name

class LibraryUsers(models.Model):
    user_id = models.OneToOneField(User, on_delete=models.CASCADE)
    first_name = models.CharField(max_length=50, blank=True)
    last_name = models.CharField(max_length=50, blank=True)
    usn_number = models.CharField(max_length=20, blank=True)
    category = models.CharField(max_length=10, blank=True)
    group = models.CharField(max_length=10, blank=True)
    dob = models.DateField(blank=True)
    gender = models.CharField(max_length=5, blank=True)
    email = models.CharField(max_length=50, blank=True)
    telephone = models.CharField(max_length=20, blank=True)
    first_time = models.BooleanField(default=False)

    def __unicode__(self):
<<<<<<< HEAD
        return self.first_name
=======
        return self.email
>>>>>>> 63c065cc10915dc0e85e12fc0f4c82342e41edc4


class Counter(models.Model):
    page_name = models.CharField(max_length=200)
    counter = models.IntegerField()

    def __unicode__(self):
        return self.page_name

class Quotes(models.Model):
    quoter = models.CharField(max_length=50, null=True)
    quote_text = models.TextField()
    quote_image_path = models.CharField(max_length=100)
    image_dimension = models.TextField(null=True)
    
    def __unicode__(self):
        return self.quote_text


class LibraryEbooks(models.Model):

    dept_id = models.ForeignKey(DeptId, on_delete=models.CASCADE)

    ebook_name = models.CharField(max_length=500)
    ebook_file_location = models.CharField(max_length=550)
    ebook_thumbnail_image_path = models.CharField(max_length=100, null=True)

    def __unicode__(self):

        return self.ebook_name


class UserFiles(models.Model):

    username = models.ForeignKey(User, on_delete=models.CASCADE)
    file_name = models.CharField(max_length=100)
    file_location = models.CharField(max_length=150)
    private_file = models.BooleanField(default=False)
    short_url = models.CharField(max_length=50, default=False)

    def __unicode__(self):

        return self.file_name