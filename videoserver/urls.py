from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'^schools/$', views.schools, name='schools'),
    url(r'^about/$', views.about, name='about'),
    url(r'^membership/$', views.membership, name='membership'),
    url(r'^learning_resources/$', views.learning_resources, name='learning_resources'),
    url(r'^nptel/$', views.nptel, name="nptel"),
    url(r'^nptel\/[0-9]{3}$', views.dept_page, name='dept_page'),
    url(r'^nptel/[0-9]{9}$', views.video_player_page, name='course_id'),
    url(r'^nptel/[0-9]{9}\/[0-9]{1,}', views.any_video_player_page, name='video_player_page'),
    url(r'get_quote/', views.get_quote, name='get_quote'),
    url(r'^login/$', views.login_user, name='login'),
    url(r'^home/$', views.home, name='home'),
    url(r'^logout/$', views.logout_user, name='logout'),
    url(r'^knimbus/$', views.knimbus, name='knimbus'),
    url(r'^feedback/$',views.feedback, name='feedback'),
    url(r'^online_db/$', views.online_db, name="online_db"),
    url(r'^contact/$',views.contact, name='contact'),
    url(r'^instm/$',views.instm, name='InstM'),
    url(r'^journals/$',views.journals, name='Journals'),
    url(r'^edu_url/$',views.edu_url, name='EduUrl'),
    url(r'^opac/$',views.opac, name='OPAC'),
    url(r'^cd/$',views.cd, name='CD'),
    url(r'^ejournals/$',views.ejournals, name='ejournals'),
    url(r'^search_results/$', views.search_results, name='search_results'),
<<<<<<< HEAD
    url(r'^complete_profile/$', views.complete_profile, name='complete_profile'),
=======
    url(r'^ebooks/$', views.ebooks, name='ebooks'),
    url(r'^ebooks\/[0-9]{3}$', views.ebooks_dept, name='ebooks_dept'),
    url(r'^upload_file/$', views.upload_file, name='upload_file'),
    url(r'^notes/$', views.show_notes, name='show_notes'),
>>>>>>> 63c065cc10915dc0e85e12fc0f4c82342e41edc4
]