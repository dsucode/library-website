from django.shortcuts import render, HttpResponse, redirect
from django.http import JsonResponse, HttpRequest
from django.contrib.auth.decorators import login_required
<<<<<<< HEAD
from videoserver.models import DeptId, Course, Topics, Counter, Quotes, LibraryUsers
=======
from videoserver.models import DeptId, Course, Topics, Counter, Quotes, Users, LibraryEbooks, UserFiles, LibraryUsers
>>>>>>> 63c065cc10915dc0e85e12fc0f4c82342e41edc4
from random import randint
from videoserver.common.CommonClass import CounterFunctions, slack_webhook, SearchLayer
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login, logout
from common.send_mail import send_mail
from common.login_class import LoginUser
from common.file_upload import FileUpload
from common.manage_home import ManageHome

import os

# Create your views here.

@login_required(login_url="../login/")
def knimbus(request):
    #slack_webhook(request)
    if request.user.is_authenticated():
        return HttpResponse("It is working")

@login_required(login_url='../login/')
def complete_profile(request):
    #slack_webhook(request)
    if request.user.is_authenticated():
        #here we need to check data according to what user has sent using get data and post data
        #just printing here data in both cases

        if request.method == 'POST':

            if 'first_name' and 'last_name' and 'usn_number' and 'category' and 'group' and 'dob' and 'gender' and 'telephone' in request.POST.keys():

                new_user = LibraryUsers()
                new_user.user_id = User.objects.get(username=request.user.username)
                new_user.first_name = request.POST['first_name']
                new_user.last_name = request.POST['last_name']
                new_user.usn_number = request.POST['usn_number']
                new_user.category = request.POST['category']
                new_user.group = request.POST['group']
                new_user.dob = request.POST['dob']
                new_user.gender = request.POST['gender']
                new_user.email = request.user.email
                new_user.telephone = request.POST['telephone']
                new_user.first_time = True
                new_user.save()

                return HttpResponse("Thank You. Your profile is complete.")
            else:
                return HttpResponse("Partial data is applied.")

def index(request):

    counter = CounterFunctions()
    counter_value = counter.get_counter(request.path)
    #slack_webhook(request)
    return render(request, 'html/index.html', {'counter_value':counter_value})

def login_user(request):

    #slack_webhook(request)

    if request.method == "POST":
<<<<<<< HEAD
        user = authenticate(username=request.POST['username'], password=request.POST['password'])
        if user is not None:
            if user.is_active:

                login(request, user)

                #insert logic to fill up profile and then continue.
                #if user is having first time then redirect to complete details page.
                if len(LibraryUsers.objects.filter(user_id__username=user.username)) == 0:

                    return render(request, 'html/complete_profile.html',{
                        'username' : user.username,
                        'email_address' : user.email
                    })
=======
        login_user = LoginUser(request)

        return login_user.login_user() # this will take care of every shit.

    else:
        return render(request, 'html/login.html', {})

def home(request):

    manage_home = ManageHome(request)

    return manage_home.check_conditions()

def show_notes(request):

    return render(request, 'html/show_notes.html', {
        'pdf_files' : UserFiles.objects.all()
    })

def upload_file(request):
>>>>>>> 63c065cc10915dc0e85e12fc0f4c82342e41edc4

    if request.user.is_authenticated and request.method == "POST" and request.FILES['pdfFile']:

        file_upload = FileUpload(request)

        return file_upload.check_file()

        # some more logic to write and return response finally

    else:
        return redirect('../login')

    pass


def logout_user(request):

    logout(request)
    return redirect('../')

def schools(request):

    counter = CounterFunctions()
    counter_value = counter.get_counter(request.path)
    #slack_webhook(request)
    return render(request, 'html/schools.html', {'counter_value':counter_value})

def about(request):

    counter = CounterFunctions()
    counter_value = counter.get_counter(request.path)
    #slack_webhook(request)
    return render(request, 'html/about.html', {'counter_value':counter_value})

def membership(request):

    counter = CounterFunctions()
    counter_value = counter.get_counter(request.path)
    print counter_value
    #slack_webhook(request)
    return render(request, 'html/membership.html', {'counter_value':counter_value})

def learning_resources(request):

    counter = CounterFunctions()
    counter_value = counter.get_counter(request.path)
    #slack_webhook(request)

    if request.user.is_authenticated():
        print request.user.username

    return render(request, 'html/learning_resources.html',  {'counter_value':counter_value})

def nptel(request):

    counter = CounterFunctions()
    counter_value = counter.get_counter(request.path)
    #slack_webhook(request)
    return render(request, 'html/nptel.html', {'counter_value':counter_value})

def online_db(request):

    counter = CounterFunctions()
    counter_value = counter.get_counter(request.path)
    #slack_webhook(request)
    return render(request, 'html/online_db.html', {'counter_value':counter_value})

def dept_page(request):

    counter = CounterFunctions()
    counter_value = counter.get_counter(request.path)
    #slack_webhook(request)
    print request.path[-3:]
    return render(request, 'html/dept_page.html',
    {'CourseId' : Course.objects.filter(dept_id__nptel_id=int(request.path[-3:])),
    'counter_value':counter_value})

def video_player_page(request):

    counter = CounterFunctions()
    counter_value = counter.get_counter(request.path)
    
    courseId = request.path.split('/')[2]
    
    topics = Topics.objects.filter(course_id__course_id=int(courseId)).order_by('id')
    first_topic = topics[0]
    #slack_webhook(request)
    return render(request, 'html/video_player.html', {
        'first_topic_object' : first_topic,
        'other_topic_objects' : topics,
        'counter_value':counter_value})

def any_video_player_page(request):

    counter = CounterFunctions()
    counter_value = counter.get_counter(request.path)
    
    split_path = request.path.split('/')
    courseId = split_path[2]
    topicId = split_path[3]

    selected_topic = Topics.objects.get(id=int(topicId))
    selected_course = Topics.objects.filter(course_id__course_id=int(courseId)).order_by('id')
    #slack_webhook(request)
    return render(request, 'html/video_player1.html', {
        'first_topic_object' : selected_topic,
        'other_topic_objects' : selected_course,
        'counter_value':counter_value})

def get_quote(request):

    quote_id = randint(1,3)
    quote_object = Quotes.objects.get(id=quote_id)

    json_data = {
        "quoter" : quote_object.quoter,
        "quote_text" : quote_object.quote_text,
        "quote_image_path" : quote_object.quote_image_path
    }

    return JsonResponse(json_data, safe=True)

def contact(request):

    counter = CounterFunctions()
    counter_value = counter.get_counter(request.path)
   
    #slack_webhook(request)
    return render(request, 'html/contact.html', {'counter_value':counter_value})

def instm(request):

    counter = CounterFunctions()
    counter_value = counter.get_counter(request.path)   
    #slack_webhook(request)
    return render(request, 'html/InstM.html', {'counter_value':counter_value})

def journals(request):

    counter = CounterFunctions()
    counter_value = counter.get_counter(request.path)   
    #slack_webhook(request)
    return render(request, 'html/Journals.html', {'counter_value':counter_value})

def edu_url(request):

    counter = CounterFunctions()
    counter_value = counter.get_counter(request.path)   
    #slack_webhook(request)
    return render(request, 'html/edu_url.html', {'counter_value':counter_value})

def opac(request):

    counter = CounterFunctions()
    counter_value = counter.get_counter(request.path)   
    #slack_webhook(request)
    return render(request, 'html/OPAC.html', {'counter_value':counter_value})

def cd(request):

    counter = CounterFunctions()
    counter_value = counter.get_counter(request.path)   
    #slack_webhook(request)
    return render(request, 'html/CD.html', {'counter_value':counter_value})

def ejournals(request):

    counter = CounterFunctions()
    counter_value = counter.get_counter(request.path)   
    #slack_webhook(request)
    return render(request, 'html/ejournals.html', {'counter_value':counter_value})

def feedback(request):

    counter = CounterFunctions()
    counter_value = counter.get_counter(request.path)
    #slack_webhook(request)
    form_data = {}
    if request.method == "POST":
        if 'first-name' in request.POST.keys():
            form_data['first_name'] = request.POST['first-name']

        if 'last-name' in request.POST.keys():
            form_data['last_name'] = request.POST['last-name']

        if 'usn' in request.POST.keys():
            form_data['usn'] = request.POST['usn']

        if 'email' in request.POST.keys():
            form_data['email'] = request.POST['email']

        if 'message' in request.POST.keys():
            form_data['message'] = request.POST['message']

        if 'branch' in request.POST.keys():
            form_data['branch'] = request.POST['branch']


        send_mail(form_data)
        return render(request, 'html/feedback.html', {
            'email_success_message' : 'Thanks for sharing your feedback about the DSU Digital Library. Your comments are appreciated!'
        })

    return render(request, 'html/feedback.html', {'counter_value' : counter_value})

def search_results(request):

    counter = CounterFunctions()
    counter_value = counter.get_counter(request.path)

    if request.method == "GET":
        search_result = SearchLayer(request.GET['search_query'])
        search_obj = search_result.get_data()
    return render(request, 'html/search_results.html', {
        'search_query' : request.GET['search_query'],
        'courses' : search_obj['courses'],
        'topics' : search_obj['topics'],
        'counter_value' : counter_value
    })

def ebooks(request):

    return render(request, 'html/ebooks.html', {})

def ebooks_dept(request):
    ebook_dept_id = request.path.split('/')[-1]
    ebooks = LibraryEbooks.objects.filter(dept_id__nptel_id=ebook_dept_id)
    return render(request, 'html/ebooks_dept.html', {
        'title_name' : ebooks[0].dept_id,
        'ebooks_list' : ebooks
    })