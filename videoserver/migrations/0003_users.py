# -*- coding: utf-8 -*-
# Generated by Django 1.10.4 on 2017-01-30 12:03
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('videoserver', '0002_topics_new_video_location'),
    ]

    operations = [
        migrations.CreateModel(
            name='Users',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=100)),
                ('id_code', models.CharField(max_length=10)),
                ('category', models.CharField(max_length=10)),
                ('group', models.CharField(max_length=5)),
                ('date_of_birth', models.CharField(max_length=10)),
                ('gender', models.CharField(max_length=2)),
                ('email', models.CharField(max_length=50)),
                ('telephone', models.CharField(max_length=20)),
            ],
        ),
    ]
