# -*- coding: utf-8 -*-
# Generated by Django 1.10.4 on 2017-01-30 12:05
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('videoserver', '0003_users'),
    ]

    operations = [
        migrations.AlterField(
            model_name='users',
            name='date_of_birth',
            field=models.CharField(max_length=20),
        ),
        migrations.AlterField(
            model_name='users',
            name='gender',
            field=models.CharField(max_length=5),
        ),
        migrations.AlterField(
            model_name='users',
            name='group',
            field=models.CharField(max_length=10),
        ),
        migrations.AlterField(
            model_name='users',
            name='id_code',
            field=models.CharField(max_length=20),
        ),
    ]
