from django.views.generic import View
from django.shortcuts import HttpResponse, redirect
from videoserver.common.file_upload import FileUpload

class ManageFile(View):

    def get(self, request):

        if request.user.is_authenticated():

            file_id = request.GET['file_id']

            delete_file = FileUpload(request)

            delete_file.delete_file(file_id)

            return redirect('../../../home')

        else:
            HttpResponse("No file name given")