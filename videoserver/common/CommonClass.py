import os
from videoserver.models import Counter
import json
import requests
from videoserver.models import Course, Topics
from django.db.models import Q

class CounterFunctions:

    def get_counter(self, url_path):
        print "inside here"

        if len(Counter.objects.filter(page_name=url_path)):
            return self.increment_counter(url_path)
        else:
            self.create_counter(url_path)


    def create_counter(self, url_path):
        print "inside create counter"
        new_counter = Counter()
        new_counter.page_name = url_path
        new_counter.counter = 1
        new_counter.save()

        return 1

    def increment_counter(self, url_path):
        counter = Counter.objects.get(page_name=url_path)
        counter_value = counter.counter
        counter.counter = counter_value + 1
        counter.save()

        return counter.counter

class GetDirInfo:

    def __init__(self, dir_path):
        self.dir_path = dir_path

    def get_dir_size(self):

        total_size = 0
        for dirpath, dirnames, filenames in os.walk(self.dir_path):
            for f in filenames:
                fp = os.path.join(self.dir_path, f)
                total_size += os.path.getsize(fp)
        return (total_size / (1024 * 1024))


def slack_webhook(request):

    slack_webhook_url = "https://hooks.slack.com/services/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"

    payload = {
        "title" : "Request recieved from client",
        "text" : [request.META['REMOTE_ADDR'], request.path],
        "attachments" : [
            {
                "text" : "Accessed url: " + request.path
            },
            {
                "text" : "remote address: " + request.META['REMOTE_ADDR']
            }
        ]
    }

    requests.post(slack_webhook_url, json.dumps(payload))

class LoginLayer:

    pass

class SearchLayer:

    def __init__(self, search_query):

        self.search_query = search_query
        self.search_dict = {}


    def get_data(self):
        self.search_dict['courses'] = Course.objects.filter(Q(course_name__icontains=self.search_query))

        self.search_dict['topics'] = Topics.objects.filter(Q(topic_name__icontains=self.search_query))

        return self.search_dict