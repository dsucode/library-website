from django.shortcuts import redirect, render
from videoserver.models import UserFiles, LibraryUsers
from videoserver.common.CommonClass import GetDirInfo
import os

class ManageHome:

    def __init__(self, request):

        self.username = request.user.username
        self.password = request.user.password
        self.request = request
        self.dir_name = ''
        self.total_dir_size = 0


    def send_to_home(self):

        return render(self.request, 'html/home.html', {
            'user' : LibraryUsers.objects.get(),
            'pdf_files' : UserFiles.objects.filter(username__username=self.request.user.username),
            'storage_usage' : self.total_dir_size

        })

    def check_conditions(self):

        if self.request.user.is_authenticated():

            self.dir_name = 'videoserver/static/user_files/' + self.request.user.username + '/'

            if not os.path.exists(self.dir_name):
                os.makedirs(self.dir_name)

            total_size_obj = GetDirInfo(self.dir_name)
            self.total_dir_size = total_size_obj.get_dir_size()

            return self.send_to_home()

        else:
            redirect('../login')
