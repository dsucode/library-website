from django.shortcuts import render, HttpResponse, redirect
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login, logout

class LoginUser:

    def __init__(self, request):

        self.username = request.POST['username']
        self.password = request.POST['password']
        self.request = request

    def login_user(self):

        user = authenticate(username=self.username, password=self.password)

        if user is not None:
            login(self.request, user)
            #redirecting user here to home page
            #create a user home page where user can upload pdf docs.
            return redirect('../home')

        else:
            #redirecting user to error in login
            return render(self.request, 'html/login.html', {
                "login_error_warning" : "either username or password is incorrect. Please enter correct credentials or contact system administrator."
            })