from django.shortcuts import HttpResponse, render, redirect
from django.conf import settings
from django.core.files.storage import FileSystemStorage
from videoserver.models import UserFiles
from django.contrib.auth.models import User
import os

class FileUpload:

    def __init__(self, request):

        self.request = request # see for file name
        # 1.we will check weather user is auth, 2. check file size in beginning, 3. according to user quota, check weather file could be uploaded


    def check_file(self):


        if (self.request.FILES['pdfFile'].size / (1024 * 1024)) < 300 and self.request.FILES['pdfFile'].content_type == 'application/pdf':

            self.file_upload()

            save_file = UserFiles()
            save_file.username = User.objects.get(username=self.request.user.username)
            save_file.file_name = self.request.FILES['pdfFile'].name
            save_file.file_location = 'user_files/' + self.request.user.username + '/' + self.request.FILES['pdfFile'].name.replace(' ', '_')
            save_file.private_file = False

            save_file.save()

            return redirect('../home/')



        #TODO import simple pdf reader lib and read file, if uncessful report file as corrupted

        else:
            return render('html/home.html', {
                "file_issue" : "file size exceed or uploading wrong file"
            })


    def file_upload(self):

        myfile = self.request.FILES['pdfFile']
        fs = FileSystemStorage(location='videoserver/static/user_files/' + self.request.user.username)
        fs.save(myfile.name.replace(' ', '_'), myfile)


    def delete_file(self, file_id):

        file_obj = UserFiles.objects.filter(id=file_id, username__username=self.request.user.username)

        if len(file_obj) != 0:

            os.remove('videoserver/static/' + file_obj[0].file_location)

            UserFiles.objects.get(id=file_id, username__username=self.request.user.username).delete()
